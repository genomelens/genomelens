"""
Django settings for genomelens project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
MEDIA_ROOT = os.path.join(BASE_DIR, '')
MEDIA_URL = '/var/www/project/input/'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'fv58beh-ckevl+v)j0h+i&h(%3il_)fypj%cxkb_feb(4p1lvm'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

# RabbitMQ
AMQP_PROJ_NAME = "genomelens"
AMQP_HOST = "localhost"
AMQP_PORT = 5672
AMQP_VHOST = "test_vhost"
AMQP_USER = "test"
AMQP_USER_PASSWORD = "test1"
AMQP_URL = "amqp://" + AMQP_USER + ":" + AMQP_USER_PASSWORD + "@" + AMQP_HOST + ":" + str(AMQP_PORT) + "/" + AMQP_VHOST 

# Application definition
file = open('applications.txt')
apps = tuple([x.strip() for x in file])
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.humanize',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.admindocs',
    'genomelens',
    'ancestryapp',
    'GeneViewApp',
    'vcfstats',
    'registration',
    'GeminiApp',
    'celery',
    'demoapp',
    'skeleton',
) + apps

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'genomelens.urls'

WSGI_APPLICATION = 'genomelens.wsgi.application'

TEMPLATE_DIRS = (
    'C:/Python27/genomelens/genomelens/templates',
    )

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

ACCOUNT_ACTIVATION_DAYS = 7
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
DFAULT_FROM_EMAIL = 'pphaneuf@eng.ucsd.edu'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'  # I believe this is only useful for debugging.

apps = tuple([os.path.join(BASE_DIR, (x + STATIC_URL)) for x in apps])
STATICFILES_DIR = (
    os.path.join(BASE_DIR, 'genomelens/static/'),
    os.path.join(BASE_DIR, 'GeminiApp/static/'),
    os.path.join(BASE_DIR, 'ancestryapp/static'),
    os.path.join(BASE_DIR, 'vcfstats/static'),
    ) + apps

LOGIN_REDIRECT_URL = '/mainmenu/'
SITE_ID = 1

"""
# File upload
FILE_UPLOAD_HANDLERS = (
    "progressbarupload.uploadhandler.ProgressBarUploadHandler",
    "django.core.files.uploadhandler.MemoryFileUploadHandler",
    "django.core.files.uploadhandler.TemporaryFileUploadHandler",
)
"""
