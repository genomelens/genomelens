from django.conf.urls import *
from django.contrib import admin
from genomelens import views
from genomelens.views import RegistrationView
admin.autodiscover()   # may no longer need this with django 1.7

file = open('applications.txt')
apps = [url('^'+x.strip()+'/$', x.strip()+'.views.run') for x in file]

# TODO: remove obsolete comments.
urlpatterns = patterns('',

    url(r'^$', views.apphome, name='home'),
    url(r'^uploadfile', 'genomelens.views.uploadhome1', name='test'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^success/url/$', views.success),
    url(r'^search-form/$', views.search_form),
    url(r'^search/$', views.search),
    url(r'^contact-form/$', views.contact),
    url(r'^uploadView/$', views.uploadView, name='uploadFile'),
    url(r'^someview/$', views.some_view),
    url(r'^texthome/$', views.home_page),
    url(r'^csvhome/$', views.uploadhome),
    url(r'^statinfo/$', 'genomelens.views.stat_info'),
    url(r'^demoapp/', include('demoapp.urls')),
    #url(r'^login/', 'django.contrib.auth.views.login'),
    url(r'^login/', views.user_login),
    url(r'^logout/', views.user_logout),
    url(r'^register/$', views.register),
    #url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page' : '/login'}),
    url(r'^mainmenu/$', 'genomelens.views.mainmenu'),
    url(r'^support/$', views.support),
    url(r'^about/$', views.about),
    url(r'^resetps/$', views.resetps),

    #url(r'^app1/$', 'ancestryapp.views.ancestry_index'),
    ##url(r^geneview/$', 'GeneViewApp.urls'),
    (r'^geneview/', include('GeneViewApp.urls')),
    url(r'^vcfstats/$', 'vcfstats.views.run'),
    (r'^app1/', include('ancestryapp.urls')),   #   TODO: make this URL more appropriate.

    #url(r'^accounts/', include('registration.urls')),
    #url(r'^registration/', include('registration.backends.default.urls')),
    url(r'^download/', views.download),

    url(r'^gemini/', include('GeminiApp.urls')),
    url(r'^skeleton/', include('skeleton.urls')),
    *apps
)
