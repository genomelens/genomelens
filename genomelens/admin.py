from django.contrib import admin
from genomelens.models import UserProfile

# Register your models here.

admin.site.register(UserProfile)
class Document(admin.ModelAdmin):
    change_form_template = 'progressbarupload/change_form.html'
    add_form_template = 'progressbarupload/change_form.html'

