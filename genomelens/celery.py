from __future__ import absolute_import

import os
from celery import Celery
from django.conf import settings


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'genomelens.settings')


f = open('applications.txt')
template_apps = f.read().split()
f.close()
includable = ['genomelens.tasks'] + [n+'.views' for n in template_apps]

app = Celery(settings.AMQP_PROJ_NAME,
             broker=settings.AMQP_URL,
             backend='amqp://',
             include=includable)

# Optional configuration, see the application user guide.
app.conf.update(
    CELERY_TASK_RESULT_EXPIRES=3600,
    CELERY_TASK_SERIALIZER='json',
    CELERY_ACCEPT_CONTENT=['json'],  # Ignore other content
    CELERY_RESULT_SERIALIZER='json',
    CELERY_TIMEZONE='America/Los_Angeles',
    CELERY_ENABLE_UTC=True,
)

if __name__ == '__main__':
    app.start()
