from __future__ import absolute_import

from genomelens.celery import app

from ancestryapp.ancestry_app import determine_ancestry

from GeminiApp.gemini_app import load_vcf

from demoapp.models import AppState, Document

from vcfstats.vcfstats_logic import vcf_stats
from vcfstats.models import vcfstats_data

import json
import traceback

@app.task
def geneview(**kwargs):
    return


# TODO: vcf.docfile should get you the path. Don't need it as an additional arg.
@app.task
def vcfstats(path, pk, p_val):
    vcf = Document.objects.get(id=pk)
    try:
        results = vcf_stats(open(path), p_val)
    except (KeyboardInterrupt, SystemExit):
        raise
    except Exception as inst:
        print(traceback.format_exc())
        vcf.vcfstats_state = AppState.VCF_PROCESSING_ERROR
        vcf.save()
        return
    str_ret = json.dumps(results)
    allData = vcfstats_data(identifier=pk, data=str_ret)
    allData.save()
    vcf.vcfstats_state = AppState.VCF_PROCESSED
    vcf.save()



# TODO: vcf.docfile should get you the path. Don't need it as an additional arg.
@app.task
def ancestry(path, pk):
    vcf = Document.objects.get(id=pk)
    try:
        determine_ancestry(path, pk)
        vcf.ancestry_state = AppState.VCF_PROCESSED
        vcf.save()
    except:
        vcf.ancestry_state = AppState.VCF_PROCESSING_ERROR
        vcf.save()


@app.task
def gemini_load_vcf(vcf_id,
                    vcf_name,
                    user_name):

    vcf_file = Document.objects.get(id=vcf_id)
    vcf_path = vcf_file.docfile

    error_code = load_vcf(vcf_path,
                          vcf_name,
                          user_name)

    # TODO: if error_code, handle with an AppState.ERROR
    vcf_file.variant_filtering_state = AppState.VCF_PROCESSED
    vcf_file.save()

    return error_code
