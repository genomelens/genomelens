from django.shortcuts import redirect, render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from genomelens.forms import UploadFileForm, ContactForm, AuthenticationForm
from django.contrib.auth import authenticate, login, get_user
from django.contrib.auth.decorators import login_required
from django.template import Context, loader, RequestContext
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
import xlrd
import csv

from APPNAME.models import APPNAME_data

from demoapp.models import Document, AppState
import json

from genomelens.celery import app

from APPNAME.APPNAME_logic import APPNAME


def home(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        upFile = request.FILES('file')
        context = {}
        context["uploadedFile"] = upFile.read()
        return render_to_response('upload.html', context)
    else:
        form = UploadFileForm()
    return render(request, 'form.html', {'form': form})

STR_START_RESPONSE = "APPNAME application has been started. Check back shortly."
STR_RUNNING_RESPONSE = "You have already started APPNAME. It is currently running."
STR_ERR_RESPONSE = ("APPNAME application encountered an unexpected error during previous run."
                    " It is restarting now. Check back shortly to see your results.")


def run(request):
    if(request.method=='POST'):
        f_path = request.POST['vcf_path']
        pk = request.POST['vcf_id']
        vcf = Document.objects.get(id=pk)
        app_states = vcf.template_app_states        
        user = get_user(request)
        user_name = user.username
        if('APPNAME' not in app_states):
            app_states['APPNAME'] = AppState.VCF_UNPROCESSED
            vcf.template_app_states = app_states
            vcf.save()
        cur_state = app_states['APPNAME']

        if(cur_state == AppState.VCF_UNPROCESSED):
            app_states['APPNAME'] = AppState.VCF_PROCESSING
            vcf.template_app_states = app_states
            vcf.save()
            APPNAME_task.delay(f_path, pk, user_name)
            return HttpResponse(STR_START_RESPONSE)

        if(cur_state==AppState.VCF_PROCESSING):
            return HttpResponse(STR_RUNNING_RESPONSE)

        if(cur_state == AppState.VCF_PROCESSING_ERROR):
            app_states['APPNAME'] = AppState.VCF_PROCESSING
            vcf.template_app_states = app_states
            vcf.save()
            APPNAME_task.delay(f_path, pk, user_name)
            return HttpResponse(STR_ERR_RESPONSE)

        if(cur_state==AppState.VCF_PROCESSED):
            all_data = APPNAME_data.objects.filter(identifier=pk)
            if(len(all_data)!=1):
                err_str = ('APPNAME app has discovered too many objects '
                           'associated with this document. The following '
                           'objects have been deleted. APPNAME is restarting '
                           'on this document.')
                print(err_str)
                for entry in all_data:
                    print(entry.pk)
                all_data.delete()
                app_states['APPNAME'] = AppState.VCF_PROCESSING
                vcf.template_app_states = app_states
                vcf.save()
                APPNAME_task.delay(f_path, pk, user_name)
                return HttpResponse(STR_ERR_RESPONSE)
            d = str(all_data[0].data)
            user = get_user(request)
            user_name = user.username
            return render_to_response('APPNAME_page.html', {'d': d,'user_name':user_name}, context_instance=RequestContext(request))

    else:
        print('This should never be printed...')


@app.task
def APPNAME_task(path, pk, user_name):
    vcf = Document.objects.get(id=pk)
    try:
        results = APPNAME(open(path), user_name)
    except (KeyboardInterrupt, SystemExit):
        raise
    except Exception:
        print(traceback.format_exc())
        vcf.APPNAME_state = AppState.VCF_PROCESSING_ERROR
        vcf.save()
        return
    str_ret = json.dumps(results)
    allData = APPNAME_data(identifier=pk, data=str_ret)
    allData.save()
    app_states = vcf.template_app_states
    app_states['APPNAME'] = AppState.VCF_PROCESSED
    vcf.template_app_states = app_states
    vcf.save()
