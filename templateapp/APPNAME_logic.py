'''
Overview : This module is used to take in a VCF file and output the collected data
		   in the form of a python dictionary that is converted to JSON.
'''

import sys
from time import time
import json

################################################################
# App Specific Function Definitions Go Here
################################################################

def APPNAME(vcf_file, user_name):
	data = {}
	################################################################
	# Your Code Goes Here
	################################################################

	# Returned data must be in a python dictionary
	# Be sure to account for JSON limitations
	return data 

# Acts like main
if(__name__=='__main__'):
	vcf_file = open(sys.argv[1])
	start = time()
	json_formatted_data = APPNAME(vcf_file)
	print(json.dumps(json_formatted_data,sort_keys=True,indent=4))
	sys.stderr.write(str(time()-start))


