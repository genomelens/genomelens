from django.db import models
from jsonfield import JSONField
import json

class APPNAME_data(models.Model):
    identifier = models.CharField(max_length=100)
    data = JSONField()

    def __str__(self):
        return json.dumps(json.loads(self.data), indent=4)
