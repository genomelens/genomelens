from django import template
import json

register = template.Library()

@register.simple_tag
def initial(vcf):
    return str(vcf.populationdata_set.all()[0].populations)

@register.simple_tag
def change(vcf, value):
    return str(vcf.populationdata_set.get(identifier=value).populations)
