#!/usr/bin/env python

from celery.task import Task
from celery.registry import tasks

import argparse
import django
import glob
import json
import os
import subprocess
import itertools as it

#os.environ['DJANGO_SETTINGS_MODULE'] = 'genomelens.settings'
#django.setup()

from models import PopulationData, VcfFile
from demoapp.models import Document


CURRENT_DIR = os.path.dirname(__file__)

pop_to_group = {"ASW":"African", "CEU":"European", "CHB":"East Asian", "CHD":"East Asian",
                "GIH":"Indian", "JPT":"East Asian", "LWK":"African", "MEX":"American",
                "MKK":"African", "TSI":"European", "YRI":"African"}

def parse_args():

    program_description = "Determine the ancestry of individuals in a .vcf file"
    parser = argparse.ArgumentParser(description=program_description)

    parser.add_argument("-f", type=str, required=True,
                       dest="vcf_file", help="Path to .vcf file")

    return parser.parse_args()


def clean_up(*patterns):
    return it.chain.from_iterable(glob.glob(pattern) for pattern in patterns)


PLINK_VCF_HALF_CALL_MODE_MISSING = 'm'


def determine_ancestry(path, pk):

    plink_args = [CURRENT_DIR + "/src/plink",
                  "--allow-extra-chr",
                  "--chr", "1-22 XY",
                  "--geno", "0.999",
                  "--out", "pedfile",
                  "--recode",
                  "--vcf", path,
                  "--vcf-filter",
                  "--vcf-half-call", PLINK_VCF_HALF_CALL_MODE_MISSING]

    plink_call = subprocess.call(plink_args)

    iadmix_args = ["python",
                   CURRENT_DIR + "/src/runancestry.py",
                   "--freq",
                   CURRENT_DIR + "/src/hapmap3.8populations.hg19",
                   "--plink", "pedfile",
                   "--out", "out"]

    iadmix_call = subprocess.call(iadmix_args)

    os.chdir("./")
    files = [file for file in glob.glob("*.ancestry")]

    for f in files:
        populations = {}
        fh = open(f, "rb")
        data = fh.readlines()
        fh.close()
        #fname = f.split(".")[1] + ".csv"
        fname = f.split(".")[1]
        ancestry = data[-1].strip().split()[:-1]
        #pop_list = [0] * 11
        for pop in ancestry:
            d = pop.split(":")
            populations[pop_to_group[d[0]] + "-" + d[0]] = d[1]
        #vcf = VcfFile.objects.get(id=pk)
        vcf = Document.objects.get(id=pk)
        vcf.populationdata_set.create(identifier=fname, populations=json.dumps(populations))

    for f in clean_up("out.*", "pedfile.*"):
        os.unlink(f)


if __name__ == "__main__":

    args = parse_args()
    path = args.vcf_file

    determine_ancestry(path)
