from django.db import models
from jsonfield import JSONField
from demoapp.models import Document

import json

class VcfFile(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class PopulationData(models.Model):
    #vcf = models.ForeignKey(VcfFile)
    vcf = models.ForeignKey(Document)
    identifier = models.CharField(max_length=100)
    populations = JSONField()
    #app_label = 'ancestryapp'

    def __str__(self):
        return "%s\n%s\n%s" % (self.vcf, self.identifier, json.dumps(json.loads(self.populations), indent=4))
