from django.shortcuts import redirect, render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from genomelens.forms import UploadFileForm, ContactForm, AuthenticationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.template import Context, loader, RequestContext

from genomelens.tasks import ancestry
from ancestryapp.models import VcfFile
from demoapp.models import Document, AppState

from ancestry_app import determine_ancestry
#from ancestryapp.tasks import Ancestry
#from django.template.context_processors import csrf
import csv
import time
import xlrd


def home(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        File = request.FILES.get('File')
        context = {}
        if File.multiple_chunks():
            context["uploadError"] = "Uploaded file is too big (%.2f MB)."
        else:
            context["uploadedFile"] = File.read()
        return render_to_response('upload.html', context)
    else:
        form = UploadFileForm()
        return render_to_response('testform.html',
                                  {'form': form},
                                  context_instance=RequestContext(request))


def view(request):
    if request.method == 'POST':

        # grab the path and primary key from the request and look up
        # the VCF file from the db
        fname = request.POST['vcf_path']
        pk = request.POST['vcf_id']
        vcf = Document.objects.get(id=pk)

        # get the current app state and handle appropriately
        current_state = vcf.ancestry_state
        if current_state == AppState.VCF_PROCESSING:
            # app is currently running ignore request
            return HttpResponse(
                "You have already started app. It is currently running.")

        elif current_state == AppState.VCF_UNPROCESSED \
                or current_state == AppState.VCF_PROCESSING_ERROR:
            # app hasn't been run or there was an error in a previous run
            # Start App task and return
            vcf.ancestry_state = AppState.VCF_PROCESSING
            vcf.save()
            result = ancestry.delay(fname, pk)  # task will handle changing state
            return HttpResponse(
                "Ancestry application has been started. You will be notified when completed.")

        else:
            # App has already been run successfully render template
            return render_to_response('ancestryview.html',
                                      {'vcf': vcf},
                                      context_instance=RequestContext(request))

    else:   # should always receive a POST request just return
        return
