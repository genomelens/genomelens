# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ancestryapp', '0004_auto_20150428_2155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='populationdata',
            name='vcf',
            field=models.ForeignKey(blank=True, to='ancestryapp.VcfFile', null=True),
        ),
    ]
