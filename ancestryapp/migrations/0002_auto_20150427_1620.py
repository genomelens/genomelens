# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('ancestryapp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ancestrydata',
            name='asw',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='ceu',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='chb',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='chd',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='gih',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='identifier',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='jpt',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='lwk',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='mex',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='mkk',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='tsi',
        ),
        migrations.RemoveField(
            model_name='ancestrydata',
            name='yri',
        ),
        migrations.AddField(
            model_name='ancestrydata',
            name='populations',
            field=jsonfield.fields.JSONField(default=None),
            preserve_default=False,
        ),
    ]
