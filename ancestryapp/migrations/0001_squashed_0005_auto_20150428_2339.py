# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    replaces = [(b'ancestryapp', '0001_initial'), (b'ancestryapp', '0002_auto_20150427_1620'), (b'ancestryapp', '0003_auto_20150427_1624'), (b'ancestryapp', '0004_auto_20150428_2155'), (b'ancestryapp', '0005_auto_20150428_2339')]

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PopulationData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('populations', jsonfield.fields.JSONField()),
                ('identifier', models.CharField(default=None, max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='VcfFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='populationdata',
            name='vcf',
            field=models.ForeignKey(blank=True, to='ancestryapp.VcfFile', null=True),
        ),
    ]
