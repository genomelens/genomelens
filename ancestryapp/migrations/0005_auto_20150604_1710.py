# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ancestryapp', '0004_auto_20150429_0004'),
    ]

    operations = [
        migrations.AlterField(
            model_name='populationdata',
            name='vcf',
            field=models.ForeignKey(to='demoapp.Document'),
        ),
    ]
