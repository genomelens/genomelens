# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ancestryapp', '0003_auto_20150428_2355'),
    ]

    operations = [
        migrations.AlterField(
            model_name='populationdata',
            name='vcf',
            field=models.ForeignKey(to='ancestryapp.VcfFile'),
        ),
    ]
