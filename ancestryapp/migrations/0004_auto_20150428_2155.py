# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ancestryapp', '0003_auto_20150427_1624'),
    ]

    operations = [
        migrations.CreateModel(
            name='VcfFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='populationdata',
            name='identifier',
            field=models.CharField(default=None, max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='populationdata',
            name='vcf',
            field=models.ForeignKey(default=None, to='ancestryapp.VcfFile'),
            preserve_default=False,
        ),
    ]
