# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('ancestryapp', '0002_auto_20150427_1620'),
    ]

    operations = [
        migrations.CreateModel(
            name='PopulationData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('populations', jsonfield.fields.JSONField()),
            ],
        ),
        migrations.DeleteModel(
            name='AncestryData',
        ),
    ]
