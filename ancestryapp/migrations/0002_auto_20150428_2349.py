# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ancestryapp', '0001_squashed_0005_auto_20150428_2339'),
    ]

    operations = [
        migrations.AlterField(
            model_name='populationdata',
            name='identifier',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='populationdata',
            name='vcf',
            field=models.ForeignKey(to='ancestryapp.VcfFile'),
        ),
    ]
