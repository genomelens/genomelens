# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AncestryData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.CharField(max_length=50)),
                ('asw', models.FloatField(default=0.0)),
                ('ceu', models.FloatField(default=0.0)),
                ('chb', models.FloatField(default=0.0)),
                ('chd', models.FloatField(default=0.0)),
                ('gih', models.FloatField(default=0.0)),
                ('jpt', models.FloatField(default=0.0)),
                ('lwk', models.FloatField(default=0.0)),
                ('mex', models.FloatField(default=0.0)),
                ('mkk', models.FloatField(default=0.0)),
                ('tsi', models.FloatField(default=0.0)),
                ('yri', models.FloatField(default=0.0)),
            ],
        ),
    ]
