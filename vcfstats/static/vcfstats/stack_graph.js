
function text_size(s){
    var textsize = Math.round(window.innerWidth*(s/1080))
    var textstring = textsize.toString().concat("px")
    return textstring
}


function draw_graph( height_total, data, x_labels, title, width_p){
    
    layers = []
    for (var layer in data){
        layer = data[layer]
        var temp = []
        var count = 0
        for(var i in layer){
            temp.push({'x':count,'y':layer[i]})
            count+=1
        }
        layers.push(temp)
    }

    var layers = d3.layout.stack()(layers),
        y_max = d3.max(layers, function(layer){ return d3.max(layer, function(d) { return d.y0 + d.y; });}),
        num_layers = data.length,
        num_elements = x_labels.length,
        screen_width = Math.floor(window.innerWidth*width_p);
        margin = {top: Math.round(height_total/10), right: Math.round(screen_width/10), bottom: 60+Math.round(height_total/10), left: Math.round(screen_width/10)},
        width = screen_width - margin.left - margin.right,
        height = 0+height_total - margin.top - margin.bottom;
        transition_time = 200;

    var x = d3.scale.ordinal()
        .domain(d3.range(num_elements))
        .rangeRoundBands([0, width], 0.2);
    
    var y = d3.scale.linear()
        .domain([0, y_max])
        .range([height,0]);
    
    var color = d3.scale.linear()
        .domain([0, num_layers-1])
        .range(["#aad", "#556"]);


    var xAxis = d3.svg.axis()
        .scale(x)
        .tickFormat(function(i) {return x_labels[i];})
        .tickSize(3)
        .tickPadding(10)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickSize(3)
        .tickPadding(5)
        .tickFormat(d3.format('s'));

    var svg = d3.select("body").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height_total)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + (60+margin.top)+ ")")
        .on('click',change)

    var layer = svg.selectAll(".layer")
        .data(layers)
        .enter().append("g")
        .attr("class", "layer")
        .style("fill", function(d, i) { return color(i); });

    var rect = layer.selectAll("rect")
        .data(function(d) { return d; })
        .enter().append("rect")
        .attr("x", function(d) { return x(d.x); })
        .attr("y", height)
        .attr("width", x.rangeBand())
        .attr("height", 0);

    var legend = svg.selectAll(".legend")
        .data(color.domain().slice().reverse())
        .enter().append("g")
        .attr("class","legend")
        .attr("transform",function(d,i){return "translate(0,"+i*20+")";})
        .style("font-size", text_size(10) );

    legend.append("rect")
        .attr("x",width-10)
        .attr("width",10)
        .attr("height",10)
        .style("fill",color);

    var temp = ['noncoding','coding']
    legend.append("text")
        .attr("x",width-24)
        .attr("y",9)
        .attr("dy",".35em")
        .style("text-anchor","end")
        .text(function(d){return temp[d];});

    rect.transition()
        .attr("y", function(d) { return y(d.y0 + d.y); })
        .attr("height", function(d) { return y(d.y0) - y(d.y0 + d.y); });
    

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class","y axis")
        .call(yAxis);

    var test = true;

    function change() {
        if(test){
            transitionGrouped(); 
            test=false;} 
        else{
            transitionStacked(); 
            test=true;
        }
    } 

    function transitionGrouped() {
        rect.transition()
        .duration(transition_time)
        .attr("x", function(d, i, j) {return x(d.x) + x.rangeBand() / num_layers * j;})
        .attr("width", x.rangeBand() / num_layers)
        .transition()
        .attr("y", function(d) { return y(d.y); })
        .attr("height", function(d) { return height - y(d.y); });
    }

    function transitionStacked() {
        rect.transition()
        .duration(transition_time)
        .attr("y", function(d) { return y(d.y0 + d.y); })
        .attr("height", function(d) { return y(d.y0) - y(d.y0 + d.y); })
        .transition()
        .attr("x", function(d) { return x(d.x); })
        .attr("width", x.rangeBand());
    }

    svg.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")  
        .style("font-size", text_size(16)) 
        .style("text-decoration", "underline")  
        .text(title);

    svg.select(".x.axis")
        .selectAll("text")
        //.attr("transform"," translate(0,15) rotate(-65)"); // To rotate the texts on x axis. Translate y position a little bit to prevent overlapping on axis line.
        .style("font-size",text_size(10));

    svg.select(".y.axis")
        .selectAll("text")
        //.attr("transform"," translate(0,15) rotate(-65)"); // To rotate the texts on x axis. Translate y position a little bit to prevent overlapping on axis line.
        .style("font-size",text_size(10));

     return svg
}

function get_chrom_data(data){
    var contig_ids = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','X','Y','O']
    var contig_map = {}
    var count = 0
    for (var x in contig_ids){
        contig_map[contig_ids[x]] = count
        count+=1
    }
    var coding_data = []
    for (var contig in contig_ids){coding_data.push(0)}
    var noncoding_data = []
    for (var contig in contig_ids){noncoding_data.push(0)}
    for (var person in data){
        for (var contig in data[person]){
            var temp = data[person][contig]
            if(!(contig in contig_map)){contig = 'O'}
            //snps
            for (var c1 in temp['snps']){
                for (var c2 in temp['snps'][c1]){
                    coding_data[contig_map[contig]]+=temp['snps'][c1][c2][1]
                    noncoding_data[contig_map[contig]]+=temp['snps'][c1][c2][0]
                }
            }
            //indels
            for (var length in temp['indels']){
                coding_data[contig_map[contig]]+=temp['indels'][length][1]
                noncoding_data[contig_map[contig]]+=temp['indels'][length][0]
            }
        }
    }
    return [contig_ids,[noncoding_data,coding_data]]
}

function get_titv_data(data){
    var ids = ['Ti','Tv']
    var coding_data = [0,0]
    var noncoding_data = [0,0]
    var bases = 'ACGT'
    for (var person in data){
        for (var contig in data[person]){
            var temp = data[person][contig]['snps']
            for(var c1 in temp){
                for(var c2 in temp[c1]){
                    if( (c1=='T' && c2=='C') || (c1=='C' && c2=='T') ){
                        coding_data[0]+=temp[c1][c2][1]
                        noncoding_data[0]+=temp[c1][c2][0]
                    }
                    else if( (c1=='G' && c2=='A') || (c1=='A' && c2=='G') ){
                        coding_data[0]+=temp[c1][c2][1]
                        noncoding_data[0]+=temp[c1][c2][0]
                    }
                    else{
                        coding_data[1]+=temp[c1][c2][1]
                        noncoding_data[1]+=temp[c1][c2][0]
                    }
                }
            }
        }
    }
    return [ids,[noncoding_data,coding_data]]
}

function get_mutation_type_data(data){
    var ids = ['snvs','deletions','insertions']
    var coding_data = [0,0,0]
    var noncoding_data = [0,0,0]
    for (var person in data){
        for (var contig in data[person]){
            var temp = data[person][contig]
            for (var c1 in temp['snps']){
                for (var c2 in temp['snps'][c1]){
                    coding_data[0]+=temp['snps'][c1][c2][1]
                    noncoding_data[0]+=temp['snps'][c1][c2][0]
                }
            }
            for (var length in temp['indels']){
                if(parseInt(length)>0){
                    coding_data[2]+=temp['indels'][length][1]
                    noncoding_data[2]+=temp['indels'][length][0]
                }
                else{
                    coding_data[1]+=temp['indels'][length][1]
                    noncoding_data[1]+=temp['indels'][length][0]
                }
            }
        }
    }
    return [ids,[noncoding_data,coding_data]]
}

function get_snv_data(data){
    var s = 'ACGT'
    var ids = []
    var id_map = {}
    var coding_data = []
    var noncoding_data = []
    var count = 0
    for (var c1 in s){
        id_map[s[c1]] = {}
        for(var c2 in s){
            if(c1!=c2){
                ids.push(s[c1]+'\u2192'+s[c2])
                id_map[s[c1]][s[c2]] = count
                coding_data.push(0)
                noncoding_data.push(0)
                count+=1
            }
        }
    }
    for (var person in data){
        for (var contig in data[person]){
            var temp = data[person][contig]['snps']
            for (var c1 in temp){
                for (var c2 in temp[c1]){
                    coding_data[id_map[c1][c2]]+=temp[c1][c2][1]
                    noncoding_data[id_map[c1][c2]]+=temp[c1][c2][0]
                }
            }
        }
    }
    return [ids,[noncoding_data,coding_data]]
}

function get_indel_data(data){
    var indels = {}
    for (var person in data){
        for (var contig in data[person]){
            var temp = data[person][contig]['indels']
            for (var length in temp){
                if(!(length in indels)){
                    indels[length]=[0,0]
                }
                indels[length][0]+=temp[length][0]
                indels[length][1]+=temp[length][1]
            }
        }
    }
    var ids = []
    var coding_data = []
    var noncoding_data = []
    for (var l in indels){ids.push(parseInt(l))}
    ids.sort(function(a,b){return a-b})
    var ret = []
    for (var i in ids){
            if( ids[i] >= -20 && ids[i] <= 20){
            ret.push(ids[i]) 
            coding_data.push(indels[ids[i]][1])
            noncoding_data.push(indels[ids[i]][0])
        }
    }

    return [ret,[noncoding_data,coding_data]]
}

function generate_graphs(data){
    var stuff = get_chrom_data(data)
    var elem1 = new draw_graph(300,stuff[1],stuff[0],'Chromosomes',0.99)
    var stuff = get_titv_data(data)
    var elem2 = new draw_graph(300,stuff[1],stuff[0],'Transition vs Transversion',0.48)    
    var stuff = get_mutation_type_data(data)
    var elem3 = new draw_graph(300,stuff[1],stuff[0],'Mutation Types',0.48)
    var stuff = get_snv_data(data)
    var elem4 = new draw_graph(300,stuff[1],stuff[0],'SNV Types',0.99)    
    var stuff = get_indel_data(data)
    var elem4 = new draw_graph(300,stuff[1],stuff[0],'Indel Lengths',0.99)

}

function gen_data(){
    function temp(){
        return {'snps':{},'indels':{'1':[160,60],'-1':[200,20], '2':[80,20], '-2':[60,40]}}
    }
    var s = {'A':0,'C':0,'G':0,'T':0}
    var data = {'Nolan':{}}
    for (var chrom = 0; chrom<23;chrom++){
        data['Nolan'][chrom] = temp();
        for (var c1 in s){
            data['Nolan'][chrom]['snps'][c1] = {}
            for (var c2 in s){
                if(c1!=c2){
                    var t = parseInt(chrom)*10+2
                    data['Nolan'][chrom]['snps'][c1][c2] = [t,10]
                }
            }
        }
    }
    chrom = 'X'
    data['Nolan'][chrom] = temp();
    for (var c1 in s){
        data['Nolan'][chrom]['snps'][c1] = {}
        for (var c2 in s){
            if(c1!=c2){
                data['Nolan'][chrom]['snps'][c1][c2] = [100,10]
            }
        }
    }    
    chrom = 'Y'
    data['Nolan'][chrom] = temp();
    for (var c1 in s){
        data['Nolan'][chrom]['snps'][c1] = {}
        for (var c2 in s){
            if(c1!=c2){
                data['Nolan'][chrom]['snps'][c1][c2] = [100,10]
            }
        }
    }
    return data
}

//data = gen_data()
generate_graphs(data)
