'''
Overview : This module is used to gather statistics for a VCF file.
Contents :
	entry_parser : Function which parsers and updates an existing data dictionary.
	is_coding : Function checks whether or not a vcf entry is for a coding mutation.
	is_genotypic : Checks a string to make sure all of its letters are in 'ACTG'
	vcf_info : Parser the header of a VCF file to get contig IDs
	vcf_smaple_iter : Iterates over the entries of a vcf file while subsetting.
	vcf_stats : gathers many statistics for a vcf file.
'''

import random
import re
import sys
from time import time
import json

def vcf_info(rf):
	'''	Reads the header of an opened VCF file. It finds all contig IDs and 
		subject fields and returns them as a pair of lists
	'''
	rf.seek(0)
	contigs = []
	subjects = []
	for line in rf:
		if(line[0:2]=='##'):
			if(line[0:13] == '##contig=<ID='):
				contigs.append(line.split(',')[0][13:])
		elif(line[0]=='#'):
			temp = line.rstrip()
			subjects = temp.split('\t')[9:]
			break
	return (subjects,contigs)

def vcf_smaple_iter(rf,sample_size):
	'''	Iterates over a random subset of entries in an opened vcf file rf. 
		Each entry gets returned as a list of fields. Each entry has 
		probability sample_size of being included in the generated subset. 
	'''
	rf.seek(0)
	for entry in rf:
		if(entry[0]!='#' and random.random()<=sample_size):
			entry = entry.split('\t')
			yield entry

def is_genotypic(s):
	for c in s:
		if(c not in 'ACTG'):
			return False
	return True

def is_coding(entry):
	'''	Parses through a vcf entry (as a list of fields) and looks at the EFF 
		field to check whether or not the mutations in the entry are coding.
	'''
	info = entry[7].split(';')
	efffield = ''
	for x in info:
		if(x[:3]=='EFF'):
			efffield = x
	if(efffield==''):
		return 0
	efffields = efffield.split(',')
	efffields = [x.split('|') for x in efffields]
	for field in efffields:
		if(field[2]):
			return 1
	return 0

def entry_parser(entry,data,subjects):
	coding_flag = is_coding(entry)
	genotypes = [entry[3]]+entry[4].split(',')
	for i in range(9,len(entry)):
		person = subjects[i-9]
		chrom = entry[0]
		if(chrom not in data[person]):
			bases = 'ACGT'
			data[person][chrom] = {'snps':{x:{y:[0,0] for y in bases if(x!=y)} for x in bases},'indels':{} }
		if(entry[8][0:2]=='GT'):
			GT = entry[i].split(':')[0]
			alleles = re.split('/|\|',GT)
			for allele in alleles:
				if(allele[0]!='.' and allele[0]!='0'):
					allele=int(allele)
					if(is_genotypic(genotypes[allele])):
						if(len(genotypes[allele])==len(genotypes[0])):
							for i,j in zip(genotypes[0],genotypes[allele]):
								if(i!=j):
									data[person][chrom]['snps'][i][j][coding_flag]+=1
						else:
							indellength = len(genotypes[allele])-len(genotypes[0])
							if(indellength in data[person][chrom]['indels']):
								data[person][chrom]['indels'][indellength][coding_flag]+=1
							else:
								data[person][chrom]['indels'][indellength] = [0,0]
								data[person][chrom]['indels'][indellength][coding_flag]+=1
					else:
						#Weird Stuff
						pass

def vcf_stats(rf,sample_size):
	subjects,contigs = vcf_info(rf)
	bases = 'ACGT'
	data = {p:{} for p in subjects}
	for entry in vcf_smaple_iter(rf,sample_size):
		entry_parser(entry,data,subjects)
	return data

#Acts like main
if(__name__=='__main__'):
	f = open(sys.argv[1])
	start = time()
	temp = vcf_stats(f,1.0)
	print(json.dumps(temp,sort_keys=True,indent=4))
	sys.stderr.write(str(time()-start))


