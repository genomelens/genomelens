from django.shortcuts import redirect, render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from genomelens.forms import UploadFileForm, ContactForm, AuthenticationForm
from django.contrib.auth import authenticate, login, get_user
from django.contrib.auth.decorators import login_required
from django.template import Context, loader, RequestContext
#from django.template.context_processors import csrf
import xlrd
import csv


from genomelens.tasks import vcfstats
from demoapp.models import Document, AppState
from vcfstats.models import vcfstats_data

def home(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        upFile = request.FILES('file')
        context = {}
        context["uploadedFile"] = upFile.read()
        return render_to_response('upload.html', context)
    else:
        form = UploadFileForm()
    return render(request, 'form.html', {'form': form})

STR_START_RESPONSE = "VCFStats application has been started. Check back shortly."
STR_RUNNING_RESPONSE = "You have already started VCFStats. It is currently running."
STR_ERR_RESPONSE = ("VCFStats application encountered an unexpected error during previous run."
                " It is restarting now. Check back shortly to see your results.")


def run(request):
    if(request.method=='POST'):
        f_path = request.POST['vcf_path']
        pk = request.POST['vcf_id']
        vcf = Document.objects.get(id=pk)
        cur_state = vcf.vcfstats_state
        percent_of_vcf_to_handle = 0.1

        if(cur_state == AppState.VCF_UNPROCESSED):
            vcf.vcfstats_state = AppState.VCF_PROCESSING
            vcf.save()
            vcfstats.delay(f_path,pk,percent_of_vcf_to_handle)
            return HttpResponse(STR_START_RESPONSE)

        if(cur_state==AppState.VCF_PROCESSING):
            return HttpResponse(STR_RUNNING_RESPONSE)

        if(cur_state == AppState.VCF_PROCESSING_ERROR):
            vcf.vcfstats_state = AppState.VCF_PROCESSING
            vcf.save()
            vcfstats.delay(f_path,pk,percent_of_vcf_to_handle)
            return HttpResponse(STR_ERR_RESPONSE)

        if(cur_state==AppState.VCF_PROCESSED):
            all_data = vcfstats_data.objects.filter(identifier=pk)
            if(len(all_data)!=1):
                err_str = ('VCFstats app has discovered too many objects associated with this document. '
                    'The following objects have been deleted. VCFstats is restarting on this document.')
                print(err_str)
                for entry in all_data:
                    print(entry.pk)
                all_data.delete()
                vcf.vcfstats_state = AppState.VCF_PROCESSING
                vcf.save()
                vcfstats.delay(f_path,pk,percent_of_vcf_to_handle)
                return HttpResponse(STR_ERR_RESPONSE)
            d = str(all_data[0].data)
            user = get_user(request)
            user_name = user.username
            return render_to_response('vcfstats_graphs.html', {'d': d,'user_name':user_name}, context_instance=RequestContext(request))
    
    else:
        print('This should never be printed...')







