from django.shortcuts import redirect, render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from genomelens.forms import UploadFileForm, ContactForm, AuthenticationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.template import Context, loader, RequestContext
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
import xlrd
import csv

# Create your views here.

def home(request):
    if request.method == 'POST':
        response = HttpResponse()
        form = UploadFileForm(request.POST, request.FILES)
        File = request.FILES.get('File')
        book = xlrd.open_workbook(file_contents = File.read())
        writer = csv.writer(response, dialect = 'excel')
        for sheet in book.sheets():
            for row in xrange(sheet.nrows):
                # for col in xrange(sheet.ncols):
                    # print sheet.cell_value(row, col)
                rv = unicode(sheet.row_values(row))
                writer.writerow(rv)
        # context["uploadedFile"] = File.read()
        # if form.is_valid():
        #    handle_uploaded_file(request.FILES['file'])
        #    return HttpResponseRedirect('/success/url/')
        return response # render_to_response('example.html', context)
    else:
        form = UploadFileForm()
        return render_to_response('testform.html', {'form': form}, context_instance=RequestContext(request))

@csrf_protect
def geneview(request):
    if request.method == 'POST':
        c = {}
        c.update(csrf(request))
        gene = request.POST.get('g', '')
        return render_to_response('geneviewer.html', {'gene': gene}, context_instance=RequestContext(request))
    else:
        with open('GeneViewApp/genes', 'rb') as f:
            genes = sorted([line.strip() for line in f])
        return render_to_response('index1.html', {'genes': genes}, context_instance=RequestContext(request))