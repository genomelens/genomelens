from django.db import models

# Create your models here.

class filedata(models.Model):
    fname = models.CharField(max_length = 30)
    text = models.TextField()
    upload_file = models.FileField(upload_to='documents')
