import argparse
import time
import random
import sys

def sample(vcf,sample):
	f = open(vcf)
	entry_count=0
	for line in f:
		if(line[0]=='#'):
			sys.stdout.write(line)
		else:
			entry_count+=1
			if(random.random()<sample):
				sys.stdout.write(line)
	sys.stderr.write('Entry Count :'+str(entry_count)+'\n\n')


if(__name__=='__main__'):
	start = time.time()
	parser = argparse.ArgumentParser(description='A simple script used to subsample a vcf.')
	parser.add_argument('vcf',help='A path to the vcf file to be subsampled')
	parser.add_argument('-s','--sample_size',help='The desired subsample rate. Acceptable values are between 0 and 1.',default=0.1,type=float)
	args = parser.parse_args()
	sample(args.vcf,args.sample_size)
	stop = time.time()
	print('Execution Time : '+str(stop-start))

