# Creates a new app template for a new developer

if [[ $1 == "" ]]; then
    echo "[ERROR] App Name must be provided"
    exit 1
fi


if [ -d "$1" ]; then
    echo "[Error] App Name already exists"
    exit 1
fi

echo $1 >> applications.txt

mkdir $1
cp -r templateapp/* $1/

( shopt -s globstar dotglob;
    for file in $1/*; do
        if [[ -f $file ]] && [[ -w $file ]]; then
            sed -i -- "s/APPNAME/$1/g" "$file"
        fi
   done
)

sed -i -- "s/APPNAME/$1/g" $1/templates/APPNAME_page.html

L="_logic.py"
D="_display.js"
P="_page.html"
mv $1/APPNAME_logic.py $1/$1$L
mv $1/static/APPNAME/APPNAME_display.js $1/static/APPNAME/$1$D
mv $1/static/APPNAME $1/static/$1
mv $1/templates/APPNAME_page.html $1/templates/$1$P

