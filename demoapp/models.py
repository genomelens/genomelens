from django.db import models
from django.contrib.auth.models import User
from django_enumfield import enum
from jsonfield import JSONField
import os


def get_upload_path(instance, filename):

    return os.path.join('input',
                        instance.owner.username,
                        filename)


class AppState(enum.Enum):

    VCF_UNPROCESSED = 0
    VCF_PROCESSING = 1
    VCF_PROCESSED = 2
    VCF_PROCESSING_ERROR = 3


class Document(models.Model):

    owner = models.ForeignKey(User,
                              blank=True,
                              null=True,
                              related_name='owner')

    docfile = models.FileField(upload_to=get_upload_path)

    content = models.CharField(max_length=2000,
                               default="")

    ancestry_state = enum.EnumField(AppState,
                                    default=AppState.VCF_UNPROCESSED)

    vcfstats_state = enum.EnumField(AppState,
                                    default=AppState.VCF_UNPROCESSED)

    variant_filtering_state = enum.EnumField(AppState,
                                             default=AppState.VCF_UNPROCESSED)

    template_app_states = JSONField(default={})

    def filename(self):

        return os.path.basename(self.docfile.name)
