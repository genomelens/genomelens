# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('demoapp', '0003_auto_20150506_2119'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='content',
            field=models.CharField(default=b'', max_length=2000),
        ),
    ]
