# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('demoapp', '0004_document_content'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='ancestry_state',
            field=models.IntegerField(default=0),
        ),
    ]
