# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('demoapp', '0005_document_ancestry_state'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='vcfstats_state',
            field=models.IntegerField(default=0),
        ),
    ]
