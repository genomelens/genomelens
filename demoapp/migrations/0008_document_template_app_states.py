# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('demoapp', '0007_document_variant_filtering_state'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='template_app_states',
            field=jsonfield.fields.JSONField(default={}),
        ),
    ]
