# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth import get_user as auth_get_user
from django.shortcuts import get_object_or_404

from demoapp.models import Document
from demoapp.forms import DocumentForm

app_file = open('applications.txt')
apps = app_file.read().split()
app_file.close()
app_paths = ['/'+a+'/' for a in apps]
apps = [(a, p) for a, p in zip(apps, app_paths)]

# Create your views here.

def demo(request):
    # Handle file upload
    if request.method == 'POST':
        user = auth_get_user(request)
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile=request.FILES['docfile'])
            newdoc.owner = auth_get_user(request) #request.user #set owner
            newdoc.content = form.cleaned_data['content']
            newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('demoapp.views.demo'))
    else:
        form = DocumentForm()   # An empty, unbound form
        user = auth_get_user(request)
        
    # Load documents for the demo page
    documents = Document.objects.filter(owner=user)

    # Render demo page with the documents and the form
    return render_to_response(
        'demoapp/demo-p.html',
        {
            'documents': documents,
            'form': form,
            'username': user.username,
            'apps' : apps
        },
        context_instance=RequestContext(request)
    )


def delete_upload(request):
    print "execution goes here"
    if request.method != 'POST':
        raise HTTP404
    print "execution goes here"
    docId = request.POST.get('docfile', None)
    docToDel = get_object_or_404(Document, pk = docId)
    docToDel.docfile.delete()
    docToDel.delete()

    return HttpResponseRedirect(reverse('demoapp.views.demo'))

#def file_list(request):
#    files = TextFile.objects.filter(request.user)
#    return render(request, 'file_list.html', {'files': files})

#def serve_file(request, file_id):
#    file = get_object_or_404(Document, user=request.user, pk=file_id)
#    doc = Document.objects.get(pk=file_id)
#    return HttpResponse(doc.content file.content.read())
