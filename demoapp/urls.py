# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('demoapp.views',
                       url(r'^demo/$', 'demo', name='demo'),
                       url(r'^delete_f/$', 'delete_upload', name='delete_f'),
                       )
