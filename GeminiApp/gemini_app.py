import subprocess


GEMINI_COMMAND = 'gemini'
GEMINI_LOAD_COMMAND = 'load'
VCF_FILE_FLAG = '-v'
# GEMINI_SKIP_CADD_OPTION = '--skip-cadd'  # TODO: remove when no longer testing.
# GEMINI_SKIP_GERP_BP_OPTION = '--skip-gerp-bp'  # TODO: remove when no longer testing.
ANNOTATION_FLAG = '-t'
SNPEFF_ANNOTATION = 'snpEff'

GEMINI_DB_FILE_EXTENSION = '.db'
OUTPUT_DIR = 'output/'


def load_vcf(vcf_path,
             vcf_name,
             user_name):

    output_db_dir = OUTPUT_DIR
    output_db_dir += user_name + '/'

    output_db_path = output_db_dir + vcf_name
    output_db_path += GEMINI_DB_FILE_EXTENSION

    gemini_load_process_command = [GEMINI_COMMAND,
                                   GEMINI_LOAD_COMMAND,
                                   # GEMINI_SKIP_CADD_OPTION,
                                   # GEMINI_SKIP_GERP_BP_OPTION,
                                   ANNOTATION_FLAG,
                                   SNPEFF_ANNOTATION,
                                   VCF_FILE_FLAG,
                                   str(vcf_path),
                                   str(output_db_path)]

    gemini_load_process = subprocess.Popen(gemini_load_process_command,
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE)

    load_output_message, load_output_error = gemini_load_process.communicate()

    return load_output_error  # TODO: Should also return error message
