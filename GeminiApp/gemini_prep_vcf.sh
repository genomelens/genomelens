# setup
VCF=/data/genomelens_data/final/snp.final.vcf
NORMVCF=/data/genomelens_data/final/norm.snp.final.vcf.gz
REF=/data/genomelens_data/final/human_g1k_v37.fasta
SNPEFFJAR=/home/pphaneuf/snpEff/snpEff.jar

# decompose, normalize and annotate VCF with snpEff.
# NOTE: can also swap snpEff with VEP
#NOTE: -classic and -formatEff flags needed with snpEff >= v4.1
zless $VCF \
   | sed 's/ID=AD,Number=./ID=AD,Number=R/' \
   | vt decompose -s - \
   | vt normalize -r $REF - \
   | java -Xmx4G -jar $SNPEFFJAR GRCh37.69 \
   | bgzip -c > $NORMVCF
tabix -p vcf $NORMVCF

gemini load --cores 3 -t snpEff -v $NORMVCF $db
