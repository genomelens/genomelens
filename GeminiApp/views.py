import os.path
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import get_user as auth_get_user

from demoapp.models import Document, AppState
from genomelens.tasks import gemini_load_vcf


# TODO: GeminiQuery was copy-pasted from the installed directory.
# Instead, should refer to the installation directory.
from GeminiApp.GeminiQuery import GeminiQuery

# TODO: This should be a value within the settings so that all apps can access it.
GENOMELENS_OUTPUT_DIR = 'output/'


def gemini(request):

    vcf_id = request.POST.get('vcf_id',
                              False)

    vcf_file = Document.objects.get(id=vcf_id)

    # TODO: 'output/' is also hardcoded in gemini_app.py;
    # should share it somehow. It refers to genomelens/output

    current_vcf_processed_state = vcf_file.variant_filtering_state

    if current_vcf_processed_state == AppState.VCF_UNPROCESSED:

        http_response = _handle_vcf_unprocessed_appstate(request,
                                                        vcf_file,
                                                        vcf_id)

    elif current_vcf_processed_state == AppState.VCF_PROCESSING:

        http_response = _handle_vcf_processing_appstate()

    elif current_vcf_processed_state == AppState.VCF_PROCESSED:

        http_response = _handle_vcf_processed_appstate(request)

    else:  # Defaults to error.

        http_response = _handle_vcf_processing_error_appstate()

    return http_response


def _handle_vcf_processing_appstate():

    return HttpResponse("Variant Filters is currently processing your VCF file.")


def _handle_vcf_processing_error_appstate():

    return HttpResponse("Processing this VCF has resulted in an error.")


def _handle_vcf_unprocessed_appstate(request, vcf_file, vcf_id):

    vcf_file.variant_filtering_state = AppState.VCF_PROCESSING
    vcf_file.save()

    vcf_name = request.POST.get('vcf_name',
                                False)

    user = auth_get_user(request)
    user_name = user.username

    gemini_db_user_dir = GENOMELENS_OUTPUT_DIR + user_name + '/'

    if not os.path.exists(gemini_db_user_dir):
        os.makedirs(gemini_db_user_dir)

    gemini_load_vcf.delay(vcf_id,
                          vcf_name,
                          user_name)

    return HttpResponse("Variant Filters is now processing your VCF file.")


def _handle_vcf_processed_appstate(request):

    user = auth_get_user(request)
    user_name = user.username

    vcf_name = request.POST.get('vcf_name', False)
    gemini_db_name = vcf_name + '.db'
    gemini_db_user_dir = user_name + '/'
    gemini_db_path = GENOMELENS_OUTPUT_DIR + gemini_db_user_dir + gemini_db_name

    gemini_query = _get_gemini_query(gemini_db_path)

    # Default values
    table_header = []
    output_header = "Please submit a query"
    show_samples = False

    # TODO: put in separate function within GeminiApp/gemini_app.py
    # TODO: replace request.POST['q'] with  request.POST.get('q', False)
    if 'q' in request.POST and request.POST['q']:

        if os.path.isfile(gemini_db_path):

            user_query = request.POST['q'].encode("ascii")

            output_header = user_query

            show_samples = _is_show_samples(request)
            
            gemini_query.run(user_query,
                             show_variant_samples=show_samples)

            table_header = _get_table_header(gemini_query)

        # TODO: I have a feeling that I shouldn't be re-passing vcf_name and
        # vcf_id to the HTML template; this was a hack for the GeminiApp to
        # retain name of the VCF file and resulting DB after every query.
        # The better way of doing this is probably by using a model.
        # GeminiApp/templates/GeminiApp/query.html also involved in this.


    return render(request, 'GeminiApp/query.html',
                  {
                      'vcf_name': request.POST.get('vcf_name', False),
                      'vcf_id': request.POST.get('vcf_id', False),
                      'user_name': user_name,
                      'show_samples': show_samples,
                      'gemini_db_name': gemini_db_name,
                      'output_header': output_header,
                      'gemini_query': gemini_query,
                      'table_header': table_header
                  })


def _is_show_samples(request):

    if request.POST.get('show-samples-checkbox') == u"show-samples":
        show_samples = True
    else:
        show_samples = False

    return show_samples


def _get_gemini_query(gemini_db_path):

    gemini_query = GeminiQuery(gemini_db_path)
    gemini_query.for_browser = True

    return gemini_query


def _get_table_header(gemini_query):

    table_header = []

    first_dict = {}

    # TODO: Hack. Need to be able to find a better way of pulling out the
    # first dictionary from the GenomeQuery object
    # so as to get the column names
    for row in gemini_query:
        first_dict = row
        break

    for key, value in first_dict.items():
        table_header.append(key)

    return table_header
